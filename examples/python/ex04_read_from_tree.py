import ROOT

f = ROOT.TFile("input.root")
t = f.Get("AnalysisTree")

jetPt = ROOT.TH1F("jetPt", "All Jets;p_{T} [MeV]", 100, 0, 200e3);
jetEtaPhi = ROOT.TH2F("jetEtaPhi", "All Jets;#eta;#phi", 10, -5, 5, 14, -3.5, 3.5)
jetEtaPhiPtWeight = ROOT.TH2F("jetEtaPhiPtWeight", "All Jets (pT weighted);#eta;#phi", 10, -5, 5, 14, -3.5, 3.5)
elePt = ROOT.TH1F("elePt", "All electrons;p_{T} [MeV]", 50, 0, 100e3)
muPt = ROOT.TH1F("muPt", "All muons;p_{T} [MeV]", 50, 0, 100e3)

for iEntry in range(t.GetEntries() ):
  t.GetEntry(iEntry)

  for iJet in range(len(t.JetPt) ):
    jetPt.Fill(t.JetPt.at(iJet) )
    jetEtaPhi.Fill(t.JetEta.at(iJet), t.JetPhi.at(iJet) )
    jetEtaPhiPtWeight.Fill(t.JetEta.at(iJet), t.JetPhi.at(iJet), t.JetPt.at(iJet) )
  for pt in t.ElectronPt:
    elePt.Fill(pt)
  for pt in t.MuonPt:
    muPt.Fill(pt)

fOut = ROOT.TFile("output.root", "RECREATE")
fOut.WriteTObject(jetPt)
fOut.WriteTObject(jetEtaPhi)
fOut.WriteTObject(jetEtaPhiPtWeight)
fOut.WriteTObject(elePt)
fOut.WriteTObject(muPt)
