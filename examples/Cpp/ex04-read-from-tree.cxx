#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <iostream>

int main() {
  TFile f("input.root");
  // Retrieve the TTree from the input
  TTree* t = nullptr;
  f.GetObject("AnalysisTree", t);
  if (!t) {
    std::cerr << "Failed to retrieve 'AnalysisTree' from input file!" << std::endl;
    return 1;
  }

  // Prepare the readers to read the branches
  TTreeReader reader(t);
  TTreeReaderArray<float> rJetPt(reader, "JetPt");
  TTreeReaderArray<float> rJetEta(reader, "JetEta");
  TTreeReaderArray<float> rJetPhi(reader, "JetPhi");
  TTreeReaderArray<float> rElePt(reader, "ElectronPt");
  TTreeReaderArray<float> rMuPt(reader, "MuonPt");

  // Prepare the output file
  TFile fOut("output.root", "RECREATE");

  TH1F jetPt("jetPt", "All Jets;p_{T} [MeV]", 100, 0, 200e3);
  TH2F jetEtaPhi("jetEtaPhi", "All Jets;#eta;#phi", 10, -5, 5, 14, -3.5, 3.5);
  TH2F jetEtaPhiPtWeight("jetEtaPhiPtWeight", "All Jets (pT weighted);#eta;#phi", 10, -5, 5, 14, -3.5, 3.5);
  TH1F elePt("elePt", "All electrons;p_{T} [MeV]", 50, 0, 100e3);
  TH1F muPt("muPt", "All muons;p_{T} [MeV]", 50, 0, 100e3);

  // Loop over the input
  while (reader.Next() ) {
    for (std::size_t ii = 0; ii < rJetPt.GetSize(); ++ii) {
      jetPt.Fill(rJetPt[ii]);
      jetEtaPhi.Fill(rJetEta[ii], rJetPhi[ii]);
      jetEtaPhiPtWeight.Fill(rJetEta[ii], rJetPhi[ii], rJetPt[ii]);
    }
    for (float pt : rElePt)
      elePt.Fill(pt);
    for (float pt : rMuPt)
      muPt.Fill(pt);
  }

  fOut.WriteTObject(&jetPt);
  fOut.WriteTObject(&jetEtaPhi);
  fOut.WriteTObject(&jetEtaPhiPtWeight);
  fOut.WriteTObject(&elePt);
  fOut.WriteTObject(&muPt);

  return 0;
}
