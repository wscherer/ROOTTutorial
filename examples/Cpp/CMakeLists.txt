# Ensure that we're using an up to date CMake version
cmake_minimum_required( VERSION 3.9 )

project( ROOTTutorial )

# You can work out which components you need by looking at the libraries used by
# each of the ROOT classes you use.
# Usually the library for each class is shown in a graphic towards the bottom of
# that class' page on the documentation. The component name is the library name
# without the 'lib' prefix.
find_package( ROOT COMPONENTS Tree Hist RIO Gpad MathCore TreePlayer )
# This is an extra file that provides some extra configuration for ROOT
include( ${ROOT_USE_FILE} )

# Define the CMake target
add_executable( ex01-draw-hist
    ex01-draw-hist.cxx
)
# Link the ROOT libraries
target_link_libraries( ex01-draw-hist
    ROOT::Hist ROOT::RIO ROOT::Gpad )

add_executable( ex02-fill-random-hist
    ex02-fill-random-hist.cxx)
target_link_libraries( ex02-fill-random-hist
    ROOT::Hist ROOT::Gpad ROOT::MathCore )

add_executable( ex03-write-random-hist
    ex03-write-random-hist.cxx)
target_link_libraries( ex03-write-random-hist
    ROOT::Hist ROOT::RIO ROOT::MathCore )

add_executable( ex04-read-from-tree
    ex04-read-from-tree.cxx)
target_link_libraries( ex04-read-from-tree
    ROOT::RIO ROOT::TreePlayer ROOT::Hist)
